import React, {useEffect, useState} from 'react';
import { urlServidor } from '../../constantes';
import {Link, useNavigate} from "react-router-dom";
import axios from "axios";
import Bici from "../../modelo/bici"

const BicisLista = () => {
    const [bicis, setBicis] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        obtenerBicis();
    }, []);


    function obtenerBicis(){
        let promesa = axios.get(`${urlServidor}/bicis`);
        promesa.then((resultado) => {
                setBicis(resultado.data);
            });
        promesa.catch((error) => {
                alert(error);
            });
    }

    const agregarBici = () => {
        navigate("-1");
    }

    function quitarBici(id) {
        axios.delete(`${urlServidor}/bicis/${id}`)
            .then((respuesta)=>obtenerBicis())
            .catch((error)=>alert(error));
    }

    function tablaBicis() {
        if (!bicis) return (<div>No hay datos</div>);
        return bicis.map((bici, idx) => (
            <tr key={idx}>
                <td><Link to={"" + bici.id}>{bici.id}</Link></td>
                <td>{bici.marca}</td>
                <td>{bici.rodado}</td>
                <td>{bici.color}</td>
                <td><button onClick={()=>quitarBici(bici.id)}>Borrar</button> </td>
            </tr>
        ))
    }

    return (
        <>
            <div className="container-fluid ">
                <h1 className="mt-3 text-center">Bicis</h1>
                <table className="table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Marca</th>
                        <th>Rodado</th>
                        <th>Color</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        tablaBicis()
                    }
                    </tbody>
                </table>
                <button className="btn btn-primary" onClick={agregarBici}>Agregar</button>
            </div>
        </>
    );
};

export default BicisLista;