import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import {borrarMoto, getMotos} from '../../servicios/datos_motos';
import axios from 'axios';

export default function MotosLista() {
    const [datos, setMotos] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        obtenerMotos();
    }, []);

    async function obtenerMotos() {
        setMotos(await getMotos());
    }

    const borrar= async (id) => {
        await borrarMoto(id);
        obtenerMotos();
    }

    function agregarMoto() {
        navigate("-1")
    }


    return (
        <>
            <div className="container-fluid ">
                <h1 className="mt-3 text-center">Motos</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Marca</th>
                            <th>Cilindrada</th>
                            <th>Color</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            datos.map((moto, idx) => (
                                <tr key={idx}>
                                    <td><Link to={"" + moto.id}>{moto.id}</Link></td>
                                    <td>{moto.marca}</td>
                                    <td>{moto.cilindros}</td>
                                    <td>{moto.color}</td>
                                    <td><button onClick={() => borrar(moto.id)}>Borrar</button></td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
                <button className="btn btn-primary" onClick={agregarMoto}>Agregar</button>
            </div>
        </>
    );

};

;