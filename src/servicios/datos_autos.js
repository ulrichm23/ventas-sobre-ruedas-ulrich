import axios from "axios";
import { urlServidor } from "../constantes";

export async function getAutos() {
    let datos_autos = [];
    try {
        let response = await axios.get(`${urlServidor}/autos`);
        datos_autos = response.data;
    } catch (e) {
        alert(e);
    }
    return datos_autos;
}

export async function borrarAuto(id){
    try{
        let response= await axios.delete(`${urlServidor}/autos/${id}`)
    }catch(e){
        alert(e)
    }
}

export async function getAuto(id){
    let datos_auto;
    try{
        let response = await axios.get(`${urlServidor}/autos/${id}`)
        datos_auto = response.data;
    }catch(e){
        alert(e)
    }
    return datos_auto;
}

export async function grabarAuto(auto){
    if (auto.id<0){
        try{
            await axios.post(`${urlServidor}/autos`, {...auto, id: null})
        }catch(e){
            alert(e);
        }
    }else{
        try{
            await axios.put(`${urlServidor}/autos/${auto.id}`, auto)
        }catch(e){
            alert(e);
        }
    }
}

//TAREA PARA EL HOGAR
// - modificar la parte de bicicletas para que trabaje con el json-server (sin el servicio)
// - completar el codigo del servicio de autos para que todas las llamadas al servidor esten en el servicio

// export function getAuto(id) {
//     return datos_autos.find(a => a.id === id) || null; // si no encuentra nada, find devuelve undefined; lo pasamos a null en ese caso
// }
//
// export function grabarAuto(auto) {
//     if (auto.id < 0) {
//         // es un nuevo auto, generar un nuevo id y agregar a la lista
//         auto.id = buscarNuevoId();
//         datos_autos.push(auto);
//     } else {
//         // auto existente, modifica los datos en el mismo elemento
//         let idx = datos_autos.findIndex(a => a.id === auto.id);
//         if (idx >= 0)
//             datos_autos[idx] = auto;
//     }
// }
//
// export function borrarAuto(id) {
//     let autoABorrar = datos_autos.findIndex(a => a.id === Number(id));
//     if (autoABorrar >= 0)
//         datos_autos.splice(autoABorrar, 1);
// }
//
// export function buscarNuevoId() {
//     return datos_autos.reduce((prev, curr) => curr.id > prev.id ? curr : prev, {id: -1}).id + 1;
// }