import axios from "axios";
import { urlServidor } from "../constantes";

export async function getBicis() {
    let datos_bicis = [];
    try {
        let response = await axios.get(`${urlServidor}/bicis`);
        datos_bicis = response.data;
    } catch (e) {
        alert(e);
    }
    return datos_bicis;
}
export async function borrarBici(id){
    try{
        let response= await axios.delete(`${urlServidor}/bicis/${id}`)
    }catch(e){
        alert(e)
    }
}

export async function getBici(id){
    let datos_bici;
    try{
        let response = await axios.get(`${urlServidor}/bicis/${id}`)
        datos_bici = response.data;
    }catch(e){
        alert(e)
    }
    return datos_bici;
}

export async function grabarBici(bici){
    if (bici.id<0){
        try{
            await axios.post(`${urlServidor}/bicis`, {...bici, id: null})
        }catch(e){
            alert(e);
        }
    }else{
        try{
            await axios.put(`${urlServidor}/bicis/${bici.id}`, bici)
        }catch(e){
            alert(e);
        }
    }
}