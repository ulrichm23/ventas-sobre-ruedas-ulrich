import axios from "axios";
import { urlServidor } from "../constantes";

export async function getMotos() {
    let datos_motos = [];
    try {
        let response = await axios.get(`${urlServidor}/motos`);
        datos_motos = response.data;
    } catch (e) {
        alert(e);
    }
    console.log(datos_motos);
    return datos_motos;
}
export async function borrarMoto(id){
    try{
        let response= await axios.delete(`${urlServidor}/motos/${id}`)
        console.log(response);
    }catch(e){
        alert(e)
    }
}

export async function getMoto(id){
    let datos_moto;
    try{
        let response = await axios.get(`${urlServidor}/motos/${id}`)
        datos_moto = response.data;
    }catch(e){
        alert(e)
    }
    return datos_moto;
}

export async function grabarMoto(moto){
    if (moto.id<0){
        try{
            await axios.post(`${urlServidor}/motos`, {...moto, id: null})
        }catch(e){
            alert(e);
        }
    }else{
        try{
            await axios.put(`${urlServidor}/motos/${moto.id}`, moto)
        }catch(e){
            alert(e);
        }
    }
}


// const datos_motos = [
//     {id: 1, marca: 'Honda', cilindros: 3, color: 'rojo'},
//     {id: 2, marca: 'Honda', cilindros: 4, color: 'verde'},
//     {id: 3, marca: 'Toyota', cilindros: 3, color: 'amarillo'},
//     {id: 4, marca: 'Siambretta', cilindros: 2, color: 'blanco'},
// ];

// export function getDatos() {
//     return datos_motos;
// }

// export function getMoto(id) {
//     return datos_motos.find(m => m.id === Number(id));
// }

// export function grabarMoto(moto) {
//     if (moto.id < 0) {
//         moto.id = buscarNuevoId();
//         datos_motos.push(moto);
//     } else {
//         let idx = datos_motos.findIndex(m => m.id === Number(moto.id));
//         if (idx >= 0) {
//             datos_motos[idx] = moto;
//         }
//     }
// }

// export function borrarMoto(id) {
//     let motoABorrar = datos_motos.findIndex(a => a.id === Number(id));
//     if (motoABorrar >= 0)
//         datos_motos.splice(motoABorrar,1);
// }

// export function buscarNuevoId() {
//     return datos_motos.reduce((prev, curr) => curr.id > prev.id ? curr : prev, {id: -1}).id + 1;
// }

