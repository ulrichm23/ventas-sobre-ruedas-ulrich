export default class Bici {
    constructor(id, marca, rodado, color) {
        this.id = id ? id : -1;
        this.marca = marca;
        this.rodado = rodado;
        this.color = color;
    }
}